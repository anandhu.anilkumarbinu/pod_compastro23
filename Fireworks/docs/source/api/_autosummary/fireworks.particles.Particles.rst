fireworks.Particles
=============================

.. currentmodule:: fireworks.particles

.. autoclass:: Particles
   :members:
   :show-inheritance:
   :special-members:
   :inherited-members:

   

      
         .. rubric:: Methods

         .. autosummary::
            :nosignatures:
            
               ~Particles.__init__
      
   