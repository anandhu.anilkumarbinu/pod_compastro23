"""
=======================================================================
ODE integrators collection data (:mod:`fireworks.nbodylib.integrators`)
=======================================================================

This module contains a collection of integrators to integrate one step of the ODE N-body problem


"""